1. Download jar from here: https://gitlab.com/Xcom/carpetinstaller/uploads/24d0753d3f9a228e9b8bbd46ce672dbe/carpetInstaller.jar
2. Place the jar in the folder where you want your server to run.
3. Run the jar with Java installed. If you don't have java installed then please install Java from here: https://adoptopenjdk.net/
4. The files will be placed in the update folder. The jar starting with Carpet is the server file.
5. Place the Carpet jar in any folder to run it as a server.